
![Iam Zen](https://s3.amazonaws.com/assets.cryptexlabs.com/iam-zen-wide-light-bg.png)

## <p align="center">Identity and Access Management Server</p>
<p align="center">Simple Identity and Access Management Server with admin UI</p>

## Key features
- User management
- Group management
- Policy management
- Permissions management
- Role management
- App management
- Organization management (TODO)
- Identity linking

## Source
https://gitlab.com/cryptexlabs/public/docker/iam-zen

https://gitlab.com/cryptexlabs/public/helm/-/tree/master/charts/iam-zen