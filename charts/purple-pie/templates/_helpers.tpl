{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "purple-pie.name" -}}
{{- default .Chart.Name .Values.microservice.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "purple-pie.fullname" -}}
{{- if .Values.microservice.fullnameOverride -}}
{{- .Values.microservice.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.microservice.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "purple-pie.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "purple-pie.labels" -}}
app.kubernetes.io/name: {{ include "purple-pie.name" . }}
app.kubernetes.io/instance: {{ include "purple-pie.name" . }}{{if .Values.microservice.service.version}}-{{ .Values.microservice.service.version }}{{ end }}
{{ if .Values.microservice.service.version }}
app.cryptexlabs.com/apiVersion: {{ .Values.microservice.service.version }}
{{ end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{- define "purple-pie.extraLabels" -}}
{{- if .Values.microservice.image.tag }}
app.kubernetes.io/version: {{ .Values.microservice.image.tag | squote }}
{{- end }}
helm.sh/chart: {{ include "purple-pie.chart" . }}
{{ end }}