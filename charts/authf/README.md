# Authf

Forward authentication service. Works well with [Traefik](https://doc.traefik.io/traefik/middlewares/forwardauth/) forward authentication but could be potentially used with other proxies that use forward authentication.

[Traefik Helm chart](https://artifacthub.io/packages/helm/traefik/traefik)

## Installation

```shell
helm repo add cryptexlabs helm.cryptexlabs.com
helm install authf cryptexlabs/authf
```

## JWT Keys
JWT keys are generated automatically during installation using RS256 encryption. If you want to use a different encryption mechanism please open an issue and then a pull request to enable a different type of encryption key generation.

## Examples

### Start forwarding requests to localhost
```shell
port-forward svc/authf-microservice-rest 8081:80
```

### Swagger documentation
http://localhost:8081/docs/v1/

### Create/Update an authentication
```shell
curl -X PUT "http://localhost:8081/api/v1/authentication/a27a7ae1-7350-46e6-87ad-2a9acf0d458f" \
-H  "accept: */*" \
-H  "Authorization: Basic YWRtaW46cGFzc3dvcmQ=" \
-H  "Content-Type: application/json" \
-H  "Accept-Language: en-US" \
--data-binary @- << EOF
{
  "providers": [
    {
      "data": {
        "username": "johndoe",
        "password": "b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3"
      },
      "type": "basic"
    }
  ],
  "token": {
    "expirationPolicy": {
      "access": {
        "length": 900
      },
      "refresh": {
        "length": 2592000
      }
    },
    "subject": "4ea27c6e-57b1-4cf7-822b-f3db8171a8bd",
    "body": {
      "scopes": [
        "user:self:all"
      ]
    }
  }
}
EOF
```

### Create an access token and refresh token pair

#### From basic authentication
```shell
curl -X POST "http://localhost:8081/api/v1/authentication/a27a7ae1-7350-46e6-87ad-2a9acf0d458f/token" \
-H  "accept: */*" \
-H  "Content-Type: application/json" \
-H  "Accept-Language: en-US" \
--data-binary @- << EOF
{
  "data": {
    "username": "johndoe",
    "password": "b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3"
  },
  "type": "basic"
}
EOF
```

#### From refresh token
```shell
curl -X POST "http://localhost:8081/api/v1/authentication/a27a7ae1-7350-46e6-87ad-2a9acf0d458f/token" \
-H  "accept: */*" \
-H  "Content-Type: application/json" \
-H  "Accept-Language: en-US" \
--data-binary @- << EOF
{
  "data": {
    "token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhMjdhN2FlMS03MzUwLTQ2ZTYtODdhZC0yYTlhY2YwZDQ1OGYiLCJ0eXBlIjoicmVmcmVzaCIsImlhdCI6MTYyNTQ5OTgyOH0.dabXJcEaOxHB3rpNTMgZriND9_wGMfc4GOSKAdDSemew1o1WdfgGUdeIZctsPT4t99EIg5JdtH-zbHU6flyk-S52cdl4bOrfieEEmvteWjtMZSAhrw226pTjWtQz0MjXvP21Iy3Hpqm_clXS3H9k6x-h7gLgrP8ISx06szGRCf-nK2EFRetJ6LK85qHPjWKbunu6wbBuER37fOfCzXWSGTZC90Fk_vYr2bihkgpAE39VDu66V_nXA_PAmzuBlqKrSemG6hZQ72db5UIMva_Il9HCaocDQB-yYaTbBfVwaNqjXTT7v0QVcBumZ5uLpORr0lhztaRPRw_tXultXxzioTD1t_tDm078RPSxgYCxN_wpjcCMAzmaPfbVxb9xth5Q5M__3D85rTnhxJ4lwevhTYDe4IZRjerIVECx8d9aAJ51ND1Atr0nfBzYv0syZ724PkYKq0tv4ejemfPgB2T7afDLL7BEkAunQWEQAC7-RVzFbcAlxLPu5BhtGO7zN0dfm13m-xaOO_ibUyiWSB-Kl_f4o4lfIIn229bVODixR-fmpIv4i0Mn6_d-WxUaoeJfzbYB45xoYQxBFbXEG_CY8wvMWxO_qk1iDDlYqGuLKy4RTT6r4iY7h_49Hod2Gm2mxub-KzUtr8_T1gdvFiS7N6lB2d7FIn_hnZxeeA1TMeo"
  },
  "type": "refresh"
}
EOF
```

### Traefik middleware example
```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: test-auth
spec:
  forwardAuth:
    address: http://authf-microservice-rest.development-1-authf.svc.cluster.local/api/v1/authenticated
```

For example above authf chart is installed in `development-1-authf` namespace.

Regardless of what type of http method is used in original request, traefik will use GET as the request method when validating the request. All of the http headers from the original request including the `Authorization` header will be forwarded to the authf service. If the `Authorization` header in the original request contains a valid jwt token then the request will be completed. If not then the 401 response body from the authf service will be returned to the client. 

See [Traefik ForwardAuth](https://doc.traefik.io/traefik/middlewares/forwardauth/) for more information.

## Source
https://gitlab.com/cryptexlabs/public/docker/authf

https://gitlab.com/cryptexlabs/public/helm/-/tree/master/charts/authf

## Supported Database types

###  Elasticsearch

Elasticsearch is the default database used with authf

#### Installation with Elasticsearch Helm chart

```shell
helm install authf cryptexlabs/authf
```

### MySQL

#### Installation with MySQL Helm chart

```shell
helm install authf cryptexlabs/authf --values values.yaml --values mysql.values.yaml
```

#### Installation with MySQL external database

```shell
helm install authf cryptexlabs/authf --values values.yaml --values external-mysql.values.yaml
```
