![Electric Mail](https://s3.amazonaws.com/assets.cryptexlabs.com/electric-mail-white-bg.png)

## <p align="center">Electric Mail</p>
**<p align="center">User email with audit trail</p>**

Send emails to end users by user ID. Configure emails as code. Easily switch email sending services.

## Key features
- Store email templates with yaml configuration
- Send transactional emails by user id and template ID
- Inject variables into emails when sending
- Integration with major email sending services
  - AWS SES
  - Mail Gun (TODO)
  - Mandrill (TODO)
  - SendGrid (TODO)
- Customizable unsubscribe page and links
- Kafka Consumers
- Rest API
- Admin UI

## Why use Electric Mail?
Electric mail provides 2 main things that you want in a transactional email solution which email senders don't provide.

### 1. An abstraction from email senders
This enables you to eliminate vendor lock in case you could get a better deal later without having to rewrite your application. 
If a provider service goes down or offers a better price, you can simply switch to another transactional email service and your application will work exactly the same.

### 2. Infrastructure as Code (IaC)
Electric mail allows you to define your templates in a source code repository instead of storing emails in the email senders. 
Marketing campaigns make sense to store in a GUI somewhere so email marketers can have direct access to them but transactional email 
is better to have version controlled. 

### Other considerations
- In a microservices context, many services may want to send emails. While they may not all have access or should have access to a user's email they would often all know what the user Id is that they want to send an email to. Electric mail stores the user ID email relationship so that your microservices can send email with a user Instead of having to store an email or look it up in another service.
- You want to have an audit trail of emails that were sent for debugging and audit purposes. _Most_ transactional email services provide this but if you switch between services, electric mail will provide your application a unified audit trail.
- Even if you use encryption at rest, production support persons will have access to your user emails. This is not ideal. Electric mail stores emails encrypted in the database and only decryptes during sending. This way anyone cannot just download your database of emails, they must also decrypt each and every email 1 at a time. If you use vault for decryption the attacker must compromise multiple systems to succeed and would still produce an audit trail (if you're using Vault).

## Source
https://gitlab.com/cryptexlabs/public/docker/electric-mail

https://gitlab.com/cryptexlabs/public/helm/-/tree/master/charts/electric-mail

## Installation notes

### Initializing vault
By default installation will fail because for very ridiculous and obscure reasons which no one has yet to identify, vault must be initialized by pounding commands on a CLI.

#### 1. exec into pod and run initialize command
```shell
kubectl exec -it electric-mail-vault-0 -- /bin/sh
vault operator init
```

Copy the token and the keys output from the command you ran above

#### 2. port forward the svc then open in browser
```shell
kubectl port-forward pod/electric-mail-vault-0 8200:8200
```

#### 3. Enter in 3 keys

#### 4. Hope you don't have to do that again

#### 5. Exec into vault pod again

```shell
kubectl exec -it electric-mail-vault-0 -- /bin/sh
```

#### 6. Run these commands

```shell
export VAULT_TOKEN="your root account token from step 1"
vault secrets enable transit
vault secrets enable -path=encryption transit
vault write -f transit/keys/emails
vault policy write electric-mail-emails -<<EOF
path "transit/encrypt/emails" {
   capabilities = [ "update" ]
}
path "transit/decrypt/emails" {
   capabilities = [ "update" ]
}
EOF
vault token create -policy=electric-mail-emails -format=json
```

#### 7. Base64 encode the `auth.client_token` from the output in last step

```shell
echo -n "your token" | base64
```

#### 8. Manually edit vault secrets

```shell
kubectl edit secret electric-mail-vault-secrets
```

#### 9. Run helm upgrade again to retry the installation